---
title: Oefenen
subtitle: Sites met wiskunde lessen en -oefeningen
comments: false
draft: false
date: 2020-11-16
---

## Voor Orientatie op ICT
Voor ieder voor wie deze lessen wat hard gaan, of die dingen nog een keer anders uitgelegd wil zien, heb ik in de lessen Khan Academy aangeraden. Hier kun je gratis cursussen volgen over verschillende wiskundige onderwerpen, met video lectures en automatisch beoordeelde toetsen. Deze onderwerpen sluiten aan op de stof die we tot zover behandeld hebben.
- [Khan Academy](https://www.khanacademy.org/)
  - [Delers en Veelvouden](https://www.khanacademy.org/math/cc-sixth-grade-math/cc-6th-factors-and-multiples)
  - [Logaritmes](https://www.khanacademy.org/math/algebra2/x2ec2f6f830c9fb89:logs)

### Lockdown Math
YouTuber [3Blue1Brown](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw) heeft tijdens het begin van de lockdown een interessante serie video's gemaakt waarin wiskundige onderwerpen worden uitgelicht. Een van de video's gaat specifiek over logaritmes, maar de hele serie is mogelijk de moeite waard (zeker als je wat wiskundige interesse hebt en/of AI wil gaan doen).
{{< youtube cEvgcoyZvB4 >}}

### Vihart
Met dank aan Henry de Lange:
{{< youtube N-7tcTIrers >}}

### Grafieken maken
In de les heb ik een aantal keer met grafieken gewerkt, die ik dynamisch plot met [Desmos](https://www.desmos.com/calculator).

### Alternatieve notatie voor logaritmen
![Triangle of Power](/triangle.png)
In deze video laat 3Blue1Brown een alternatieve notatie voor machten, wortels en logaritmen zien. De notatie is niet standaard en daarmee niet direct zinvol om aan te leren voor dagelijks gebruik, maar kan wel helpen om de verschillende herschrijf-regels die we hebben gezien een stuk makkelijker te onthouden te maken.

{{< youtube sULa9Lc4pck >}}

### Sorteeralgoritmen
Op [deze site](http://sorting.at/) kun je verschillende sorteeralgoritmen geanimeerd naast elkaar zetten om de complexiteit te vergelijken.

## Algemene voorbereiding AI
Voor wie AI overweegt is op zich geen wiskundige voorkennis nodig, maar zal wiskunde wel in veel vakken centraal staan, vaak in combinatie met programmeren. Als je je vast voor wil bereiden hierop, en/of wil zien of dit is voor jou dan kun je dit ook goed op Khan doen. Hieronder een lijst met onderwerpen die relevant kunnen zijn ter voorbereiding:

- [Trigonometry (trig functions)](https://www.khanacademy.org/math/trigonometry/unit-circle-trig-func)
- [Algebra](https://www.khanacademy.org/math/algebra)
- [Precalculus](https://www.khanacademy.org/math/precalculus)

Zeker de laatste twee zijn flinke onderwerpen, waarvan we lang niet alle onderdelen zullen behandelen en delen (vectoren en matrices) in latere vakken aan bod zullen komen. Kijk vooral wat je leuk lijkt en welke onderdelen je eventueel al kent, we gaan er zeker niet vanuit dat iedereen dit allemaal doorgewerkt heeft. Veel plezier!

## Voor de hoogvliegers
Om jezelf echt lekker uit te dagen kun je van 1 december tot de kerst meedoen met de [Advent of Code](https://adventofcode.com). Hoewel je in principe op ieder niveau mee kan doen kan je jezelf flink uitdagen. Ook veel hogerejaars en docenten AI/TI doen hier aan mee en houden interne leaderboards bij voor de competitie. Mocht er interesse zijn dan zal ik hier ook leaderboard-codes voor de klassen bijzetten.
