---
title: Video's
subtitle: Links naar opgenomen colleges op YouTube
comments: false
date: 2020-11-16
---

## Getallen
### Les 1
{{< youtube kBe0HOSKGfw >}}

{{< youtube Blb-lAOUvTU >}}

### Les 2
{{< youtube 8Uhev_RCs1I >}}
