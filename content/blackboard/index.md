---
title: Blackboard
subtitle: Afbeeldingen van het blackboard na de lessen
comments: false
date: 2020-11-16
---

## Getallen
![K12 Les 1](/ORAI-12-getallen-1.webp)
![K12 Les 2](/ORAI-12-getallen-2.webp)
![K12 Les 3](/ORAI-12-getallen-3.webp)

![K15 Les 1](/ORAI-15-getallen-1.webp)
![K15 Les 2](/ORAI-15-getallen-2.webp)
![K15 Les 3](/ORAI-15-getallen-3.webp)
